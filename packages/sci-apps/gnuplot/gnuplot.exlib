# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2015 Thomas G. Anderson <tanderson@caltech.edu>
# Released under the terms of the GNU General Public License v2

require texlive-common lua [ multibuild=false ]

export_exlib_phases src_configure src_test src_install pkg_postinst

SUMMARY="Command-line driven interactive data and function plotting utility"
DESCRIPTION="
Gnuplot supports many types of plots in either 2D and 3D. It can draw using lines, points, boxes,
contours, vector fields, surfaces, and various associated text. It also supports various specialized
plot types.
"
HOMEPAGE="http://${PN}.info"
DOWNLOADS="mirror://sourceforge/${PN}/${PNV/_/-}.tar.gz"

LICENCES="gnuplot"
SLOT="0"
MYOPTIONS="
    cairo [[ description = [ Enable cairo-based terminals ] ]]
    gd
    latex [[ description = [ Enable tikz latex support, build gnuplot latex tutorial ] ]]
    qt5 [[ description = [ Enable Qt-based terminals ] ]]
    wxwidgets [[ description = [ Enable wxwidgets-based terminals ] ]]
    X

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        qt5? ( x11-libs/qttools:5 )
        X? ( x11-proto/xorgproto )
    build+run:
        media-libs/libpng:=
        cairo? (
            dev-libs/glib:2[>=2.28]
            x11-libs/cairo[>=1.8.8]
            x11-libs/pango[>=1.22]
        )
        gd? ( media-libs/gd )
        latex? (
            app-text/texlive-core
            dev-texlive/texlive-latex
            dev-texlive/texlive-latexextra
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        qt5? (
            x11-libs/qtbase:5
            x11-libs/qtsvg:5
        )
        wxwidgets? ( x11-libs/wxGTK:2.8[>=2.8.10] )
        X? (
            x11-libs/libXaw
            x11-libs/libXmu
            x11-libs/libXt
            x11-libs/libXext
            x11-libs/libX11
        )
"

#BUGS_TO="philantrop@exherbo.org"
BUGS_TO="tanderson@caltech.edu arkanoid@exherbo.org"
REMOTE_IDS="freecode:${PN}"
UPSTREAM_DOCUMENTATION="http://${PN}.sourceforge.net/docs/gnuplot.html"
UPSTREAM_RELEASE_NOTES="http://${PN}.info/announce.${PV}"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-backwards-compatibility
    --enable-h3d-quadtree
    --enable-history-file
    --enable-objects
    --enable-raise-console
    --enable-stats
    --enable-x11-external
    --enable-x11-mbfonts
    --with-lua
    --with-readline=gnu
    --with-texdir=/usr/share/texmf-site
    --without-ggi
    --without-linux-vga
    --without-xmi
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( wxwidgets )

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    cairo
    gd
    "latex"
    "latex tutorial"
    "X x"
    "qt5 qt qt5"
)

DEFAULT_SRC_INSTALL_PARAMS=( PLAINTEX=no )

WORK=${WORKBASE}/${PNV/_/-}
CFLAGS+=" -DEXHERBO_REVISION=\"${PR}\"" \

gnuplot_src_configure() {
    export LUA_CFLAGS=$(${PKG_CONFIG} --cflags lua-$(lua_get_abi))
    export LUA_LIBS=$(${PKG_CONFIG} --libs lua-$(lua_get_abi))

    # Qt >= 5.7 requires at least C++11
    option qt5 && export CXXFLAGS="${CXXFLAGS} -std=c++11"

    default
}

gnuplot_src_test() {
    # Avoid test failures due to trying to access a potentially running X.
    export GNUTERM=unknown

    emake -j1 check
}

gnuplot_src_install() {
    default

    if ! option X ; then
        edo rmdir "${IMAGE}"/usr/$(exhost --target)/libexec/{${PN}/$(ever range 1-2),${PN},}
    fi

    if option latex ; then
        dodoc "${WORK}"/tutorial/tutorial.dvi
    fi
}

gnuplot_pkg_postinst() {
    etexmf-update
}

