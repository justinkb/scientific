# Copyright (c) 2008 Stephen P. Becker <spbecker@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="The R project for statistical computing."
HOMEPAGE="http://www.r-project.org"
DOWNLOADS="http://cran.r-project.org/src/base/${PN}-$(ever major)/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="X cairo readline jpeg png tcltk tiff
    external-blas [[ description = [ Use external BLAS instead of internal one ] ]]
    shared [[ description = [ Build R as a shared library (results in a 10% performance penalty). ] ]]

    jpeg? ( ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        app-arch/xz
        dev-libs/pcre
        sys-libs/libgfortran:=
        X? ( x11-libs/libX11 )
        cairo? ( x11-libs/cairo )
        jpeg? (
            providers:ijg-jpeg? ( media-libs/jpeg:= )
            providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        )
        png? ( media-libs/libpng )
        readline? ( sys-libs/readline:= )
        tcltk? ( dev-lang/tk )
        tiff? ( media-libs/tiff )
        external-blas? ( virtual/blas )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    rdocdir=/usr/share/${PN}/share/doc/
    rincludedir=/usr/$(exhost --target)/${PN}/include/
    rsharedir=/usr/share/${PN}/share/
    --with-recommended-packages
    --enable-threads=posix
    --enable-nls
    --exec-prefix=/usr/$(exhost --target)
    --localedir=/usr/share/locale
    --prefix=/usr
    --without-included-gettext
    --without-ICU
    --with-system-xz
    --with-system-zlib
    --with-system-bzlib
    --with-system-pcre
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( "shared R-shlib" )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    cairo readline tcltk
    "jpeg jpeglib"
    "png libpng"
    "tiff libtiff"
    "X x"
    "external-blas blas"
)

src_configure() {
    export F77=$(exhost --tool-prefix)gfortran
    default
}

src_test() {
    unset R_HOME
    default
}

src_install() {
    default

    edo rmdir "${IMAGE}"/usr/share/${PN}/share/doc/manual
    option shared && hereenvd 60R <<EOF
LDPATH="/usr/$(exhost --target)/lib/R/lib"
R_HOME="/usr/$(exhost --target)/lib/R"
EOF
}

